package hw11;

public class FamilyOverFlownException extends RuntimeException{

    public FamilyOverFlownException(String message){
        super(message);
    }
}
