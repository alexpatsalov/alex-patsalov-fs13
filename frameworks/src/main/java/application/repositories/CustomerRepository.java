package application.repositories;

import application.entity.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Integer> {

  //  @Override
//  List<Customer> findAll();

  Optional<Customer> findByName(String name);

}
