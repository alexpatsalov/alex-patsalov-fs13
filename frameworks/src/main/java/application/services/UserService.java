package application.services;

import application.entity.Customer;
import application.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

  @Autowired
  private CustomerRepository repository;

  private UserDetails mapper(Customer customer) {
    String role = customer.getRole();
    GrantedAuthority authority = () -> role;
    return User
            .withUsername(customer.getName())
            .password(customer.getPassword())
            .authorities(authority)
            .build();
  }

  @Override
  public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
    return repository.findByName(name)
            .map(c -> mapper(c))
            .orElseThrow(() -> new UsernameNotFoundException("user " + name + " not found"));
  }
}

