package application.controllers;


import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@AutoConfigureMockMvc
class CustomerControllerTest {

  @Test
  void getOne() {
  }

  @Test
  void getAll() {
  }

  @Test
  void createOne() {
  }

  @Test
  void modify() {
  }

  @Test
  void deleteCustomer() {
  }

  @Test
  void createAccount() {
  }

  @Test
  void deleteAccount() {
  }

  @Test
  void setEmployer() {
  }
}